<?php
class Router extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('router_model');
    }
    function index(){
        $this->load->view('router_view');
    }
 
    function router_data(){
        $data=$this->router_model->router_list();
        echo json_encode($data);
    }
 
    function save(){
        $data=$this->router_model->save_router();
        echo json_encode($data);
    }
 
    function update(){

        $data=$this->router_model->update_router();
        echo json_encode($data);
    }
 
    function delete(){
        $data=$this->router_model->delete_router();
        echo json_encode($data);
    }
 
}
 