<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Router List</title>  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dataTables.bootstrap4.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/jquery.dataTables.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
</head>
<body>
<div class="container">
    <!-- Page Heading -->
    <div class="row">
        <div class="col-12">
            <div class="col-md-12">
                <h1>Router
                    <small>List</small>
                    <div class="float-right"><a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"><span class="fa fa-plus"></span> Add New</a></div>
                </h1>
            </div>
             
            <table class="table table-striped" id="mydata">
                <thead>
                    <tr>
                        <th>SapId</th>
                        <th>Host Name</th>
                        <th>LoopBack</th>
						 <th>MacAddress</th>
                        <th style="text-align: right;">Actions</th>
                    </tr>
                </thead>
                <tbody id="show_data">
                     
                </tbody>
            </table>
        </div>
    </div>
         
</div>
 
        <!-- MODAL ADD -->
            <form>
            <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Router</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">SapId</label>
                            <div class="col-md-10">
                              <input type="text" name="sapid" id="sapid" class="form-control" placeholder="SapId">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Host Name</label>
                            <div class="col-md-10">
                              <input type="text" name="hostname" id="hostname" class="form-control" placeholder="Host Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">LoopBack</label>
                            <div class="col-md-10">
                              <input type="text" name="loopback" id="loopback" class="form-control" placeholder="Loop Back">
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-md-2 col-form-label">Mac Address</label>
                            <div class="col-md-10">
                              <input type="text" name="macaddress" id="macaddress" class="form-control" placeholder="Mac Address">
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_save" class="btn btn-primary">Save</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL ADD-->
 
        <!-- MODAL EDIT -->
        <form>
            <div class="modal fade" id="Modal_Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Router</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">SapId</label>
                            <div class="col-md-10">
                              <input type="text" name="sapid_edit" id="sapid_edit" class="form-control" placeholder="SapID" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Host Name</label>
                            <div class="col-md-10">
                              <input type="text" name="hostname_edit" id="hostname_edit" class="form-control" placeholder="Host Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">LoopBack</label>
                            <div class="col-md-10">
                              <input type="text" name="loopback_edit" id="loopback_edit" class="form-control" placeholder="LoopBack">
                            </div>
                        </div>
						 <div class="form-group row">
                            <label class="col-md-2 col-form-label">Mac Address</label>
                            <div class="col-md-10">
                              <input type="text" name="macaddress_edit" id="macaddress_edit" class="form-control" placeholder="MacAddress">
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_update" class="btn btn-primary">Update</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL EDIT-->
 
        <!--MODAL DELETE-->
         <form>
            <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Router</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                       <strong>Are you sure to delete this record?</strong>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="sapid_delete" id="sapid_delete" class="form-control">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL DELETE-->
 
    
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.5.1.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/dataTables.bootstrap4.js'?>"></script>

    <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>

 
<script type="text/javascript">
    $(document).ready(function(){
        show_router(); //call function show all Router
         
        //$('#mydata').dataTable();
          
        //function show all Router
        function show_router(){
            $.ajax({
                type  : 'ajax',
                url   : '<?php echo site_url('router/router_data')?>',
                async : true,
                dataType : 'json',
                success : function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<tr>'+
                                '<td>'+data[i].sapid+'</td>'+
                                '<td>'+data[i].hostname+'</td>'+
                                '<td>'+data[i].loopback+'</td>'+
							    '<td>'+data[i].macaddress+'</td>'+
                                '<td style="text-align:right;">'+
                                    '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-sapid="'+data[i].sapid+'" data-hostname="'+data[i].hostname+'" data-loopback="'+data[i].loopback+'" data-macaddress="'+data[i].macaddress+'">Edit</a>'+' '+
                                    '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-sapid="'+data[i].sapid+'">Delete</a>'+
                                '</td>'+
                                '</tr>';
                    }

                    $('#show_data').html(html);
                }
 
            });
        }
 
        //Save Router
        $('#btn_save').on('click',function(){
            var sapid = $('#sapid').val();
            var hostname = $('#hostname').val();
            var loopback        = $('#loopback').val();
			var macaddress        = $('#macaddress').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('router/save')?>",
                dataType : "JSON",
                data : {sapid:sapid , hostname:hostname, loopback:loopback, macaddress:macaddress},
                success: function(data){
                    $('[name="sapid"]').val("");
                    $('[name="hostname"]').val("");
                    $('[name="loopback"]').val("");
					 $('[name="mcaddress"]').val("");
                    $('#Modal_Add').modal('hide');
                    show_router();
                }
            });
            return false;
        });
 
        //get data for update record
        $('#show_data').on('click','.item_edit',function(){
            var sapid = $(this).data('sapid');
            var hostname = $(this).data('hostname');
            var loopback        = $(this).data('loopback');
			var macaddress        = $(this).data('macaddress');
             
            $('#Modal_Edit').modal('show');
            $('[name="sapid_edit"]').val(sapid);
            $('[name="hostname_edit"]').val(hostname);
            $('[name="loopback_edit"]').val(loopback);
		    $('[name="macaddress_edit"]').val(macaddress);
        });
 
        //update record to database
         $('#btn_update').on('click',function(){
            var sapid = $('#sapid_edit').val();
            var hostname = $('#hostname_edit').val();
            var loopback    = $('#loopback_edit').val();
			 var macaddress   = $('#macaddress_edit').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('router/update')?>",
                dataType : "JSON",
                data : {sapid:sapid , hostname:hostname, loopback:loopback , macaddress:macaddress},
				
                success: function(data){
                    $('[name="sapid_edit"]').val("");
                    $('[name="hostname_edit"]').val("");
                    $('[name="loopback_edit"]').val("");
					$('[name="macaddress_edit"]').val("");
                    $('#Modal_Edit').modal('hide');
                    show_router();
                }
            });
            return false;
        });
 
        //get data for delete record
        $('#show_data').on('click','.item_delete',function(){
            var sapid = $(this).data('sapid');
             
            $('#Modal_Delete').modal('show');
            $('[name="sapid_delete"]').val(sapid);
        });
 
        //delete record to database
         $('#btn_delete').on('click',function(){
            var sapid = $('#sapid_delete').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('router/delete')?>",
                dataType : "JSON",
                data : {sapid:sapid},
                success: function(data){
                    $('[name="sapid_delete"]').val("");
                    $('#Modal_Delete').modal('hide');
                    show_router();
                }
            });
            return false;
        });
 
    });
 
</script>
</body>
</html>
